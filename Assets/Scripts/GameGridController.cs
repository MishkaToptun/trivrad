﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameGridController : MonoBehaviour
{
    [Header("Настройки игрового поля")]
    [SerializeField]
    int gridWidthCells=5;
    [SerializeField]
    int gridHeightCells=9;
    [SerializeField]
    int difficult = 4;
    [SerializeField]
    Vector2 cellsSize;
    [SerializeField]
    Transform tileParent;
    [SerializeField]
    GameObject tilePrefab;
    [SerializeField]
    TIleController[] gameTiles;
    [Header("Индексы перемещения")]
    [SerializeField]
    Vector2 startDragIndex;
    [SerializeField]
    Vector2 endDragIndex;

    [Header("Типы добавляемых тайлов")]
    [SerializeField]
    GameObject[] tilesTypes;
    //управление тайлами
    TIleController[,] gridCells = new TIleController[100,100];
    TIleController startDragTile;
    TIleController endDragTile;
    private void Start()
    {
        gridCells = new TIleController[100, 100];
        //tileParent.position = new Vector2(-(int)gridWidthCells / 2, -(int)gridHeightCells / 2);
        FillGrid();
       // Camera.main.transform.position=  
    }
    private void Update()
    {
        AddNewTiles();
    }
    /// <summary>
    /// Заполнение сетки тайлами
    /// </summary>
    void FillGrid()
    {
        for (int i = 0; i < gridHeightCells; i++)
        {
            AddTileLine(i);
        }
    }
    //Добавляет линию тайлов
    private void AddTileLine(int line)
    {
        for (int i = 0; i < gridWidthCells; i++)
        {
            int tileType = UnityEngine.Random.Range(0, difficult);
            Vector2 position = new Vector2(i, line);
            CreateCell(tileType,position);
        }
    }
    // Создает тайл в ячейке
    private void CreateCell(int tileType,Vector2 position2d)
    {
        var tile = Instantiate(tilesTypes[tileType]);
        tile.transform.parent = tileParent;
        TIleController controller = tile.GetComponent<TIleController>();
        controller.SetType(tileType);
        controller.indexI = (int)position2d.x;
        controller.indexJ = (int)position2d.y;
        controller.gridCells = gridCells;
        tile.transform.localPosition = position2d;
        gridCells[(int)position2d.x, (int)position2d.y] = controller;
        controller.TileClick.AddListener(TileClicked);
        controller.TileUpClick.AddListener(TileUpClicked);
        controller.TileMouseOver.AddListener(TileMouseOvered);
    }

    private void TileMouseOvered(TIleController tile)
    {
        endDragTile = tile;
    }

    private void TileUpClicked(TIleController tile)
    {
        startDragTile = tile;
        SetTileMovePosition(startDragTile, endDragTile);
    }
    //смена положения тайлов
    List<TIleController> verticalTilesMerged;
    List<TIleController> horizontalTilesMerged;
    void SetTileMovePosition(TIleController tileA,TIleController tileB)
    {

        tileA.ChangeTilePosition(tileB);

        HasDiedTiles = false;
        if (tileA.TileType == tileB.TileType)
        {
            StartCoroutine(MoveTile(tileB, tileA));
        }
        else
        {
            StartCoroutine(TestTiles(tileA, tileB));
        }


    }
    IEnumerator TestTiles(TIleController tileA, TIleController tileB)
    {

       yield return new WaitForSeconds(0.3F);
        var checkTiles = false;
        if (Math.Abs(tileA.indexI - tileB.indexI) <= 1 & Math.Abs(tileA.indexJ - tileB.indexJ) <= 1)
            checkTiles = true;
        if (checkTiles)
        {
            CheckVerticalTiles(tileA);
            CheckVerticalTiles(tileB);
            CheckHorizontalTiles(tileA);
            CheckHorizontalTiles(tileB);
            if (!HasDiedTiles)
            {
                StartCoroutine(MoveTile(tileB, tileA));
            }
        }
    }
    IEnumerator MoveTile(TIleController tileA,TIleController tileB)
    {
        yield return new WaitForSeconds(0.2F);
        tileA.ChangeTilePosition(tileB);

    }
    bool HasDiedTiles = false;
    void CheckVertAndHorizontalTiles()
    {
        if (verticalTilesMerged.Count >= 3)
        {
            foreach (TIleController tile in verticalTilesMerged)
            {
                tile.Die();
            }
            HasDiedTiles = true;
        }
        if (horizontalTilesMerged.Count >= 3)
        {
            foreach (TIleController tile in horizontalTilesMerged)
            {
                tile.Die();
            }
            HasDiedTiles = true;
        }
    }
    private void CheckVerticalTiles(TIleController tileA)
    {   
        
        verticalTilesMerged = new List<TIleController>();
        TIleController curTile = tileA;
        int startIndex = tileA.indexJ;
        verticalTilesMerged.Add(curTile);
        if (curTile != null & tileA!=null)
        {
            while (curTile.TileType == tileA.TileType)
            {
                startIndex++;
                if (startIndex > gridHeightCells)
                {
                    break;
                }
                curTile = gridCells[tileA.indexI, startIndex];
                if (curTile != null)
                {
                    if (curTile.TileType == tileA.TileType)
                    {
                        verticalTilesMerged.Add(curTile);
                    }
                }
                else
                {
                    break;
                }
               

            }
        }
        curTile = tileA;
        startIndex = tileA.indexJ;
        if (curTile != null & tileA!=null)
        {
            while (curTile.TileType == tileA.TileType)
            {
                startIndex--;
                if (startIndex < 0)
                {
                    break;
                }
                curTile = gridCells[tileA.indexI, startIndex];
                if (curTile != null)
                {
                    if (curTile.TileType == tileA.TileType)
                        verticalTilesMerged.Add(curTile);
                }
                else
                {
                    break;
                }
            }
        }
        if (verticalTilesMerged.Count >= 3)
        {
            foreach (TIleController tile in verticalTilesMerged)
            {
                tile.Die();
            }
            HasDiedTiles = true;
        }
    }
    private void CheckHorizontalTiles(TIleController tileA)
    {

        horizontalTilesMerged = new List<TIleController>();
        TIleController curTile = tileA;
        int startIndex = tileA.indexI;
        horizontalTilesMerged.Add(curTile);
        if (curTile != null & tileA != null)
        {
            while (curTile.TileType == tileA.TileType)
            {
                startIndex++;
                if (startIndex > gridWidthCells)
                {
                    break;
                }
                curTile = gridCells[startIndex,tileA.indexJ];
                if (curTile != null)
                {
                    if (curTile.TileType == tileA.TileType)
                    {
                        horizontalTilesMerged.Add(curTile);
                    }
                }
                else
                {
                    break;
                }


            }
        }
        curTile = tileA;
        startIndex = tileA.indexI;
        if (curTile != null & tileA != null)
        {
            while (curTile.TileType == tileA.TileType)
            {
                startIndex--;
                if (startIndex < 0)
                {
                    break;
                }
                curTile = gridCells[startIndex,tileA.indexJ];
                if (curTile != null)
                {
                    if (curTile.TileType == tileA.TileType)
                        horizontalTilesMerged.Add(curTile);
                }
                else
                {
                    break;
                }
            }
        }
        if (horizontalTilesMerged.Count >= 3)
        {
            foreach (TIleController tile in horizontalTilesMerged)
            {
                tile.Die();
            }
            HasDiedTiles = true;
        }
    }
    private void TileClicked(TIleController tile)
    {
    }

    void AddNewTiles()
    {
        for (int i = 0; i < gridWidthCells; i++)
        {
            if (gridCells[i, gridHeightCells] == null)
            {
                CreateCell(Random.Range(0,difficult),new Vector2(i,gridHeightCells));
            }
        }
    }
}
