﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TIleController : MonoBehaviour
{

    public int indexI;
    public int indexJ;
    [SerializeField]
    int tileType;
    public int TileType
    {
        get
        {
            return tileType;
        }
    }
    [SerializeField]
    float moveSpeed = 0.1F;
    bool selectTile = false;
    bool moveTile = false;
    public  bool MoveTile
    {
        get
        {
            return moveTile;
        }
    }
    Vector3 nextPosition;
    [SerializeField]
    GameObject deathFVX;
    public TIleController[,] gridCells;

    private void Update()
    {
        Move();
        if (indexJ > 0)
        {
            if (gridCells[indexI, indexJ - 1] == null)
            {
                SetMovePosition(indexI, indexJ - 1);
            }
        }
    }

    private void Move()
    {
        if (moveTile)
        {
            transform.localPosition = Vector2.Lerp(transform.localPosition,nextPosition,moveSpeed*Time.deltaTime);
            //округление позиции объекта при движении.
            if (Math.Abs((transform.position - nextPosition).magnitude) < 0.1)
            {
                transform.localPosition = nextPosition;
                moveTile = false;
            }
        }
    }
    public void SetType(int type)
    {
        tileType = type;
    }
    public void Die()
    {
        var deathvfx = Instantiate(deathFVX);
        deathvfx.transform.position = transform.position;
        GameObject.Destroy(this.gameObject);
        Destroy(deathvfx, 0.6F);
        gridCells[indexI, indexJ] = null;
    }
    public void SetMovePosition(Vector3 nextposition)
    {
        moveTile = true;
        nextPosition = nextposition;
        indexI = (int)nextposition.x;
        indexJ = (int)nextposition.y;
    }
    //смена тайлов местами
    public void ChangeTilePosition(TIleController tile)
    {
        if ((Mathf.Abs(tile.indexI - indexI) <= 1 & tile.indexJ == indexJ) |
                (Mathf.Abs(tile.indexJ - indexJ) <= 1 & tile.indexI == indexI))
        {
            //Debug.Log("set tile position " + indexI + " " + indexJ + "MoveTo " + tile.indexI + " " + tile.indexJ);
            tile.nextPosition = new Vector2(indexI, indexJ);
            tile.moveTile = true;
            gridCells[indexI, indexJ] = tile;
            int oldI = indexI;
            int oldJ = indexJ;
            int i = tile.indexI;
            int j = tile.indexJ;
            moveTile = true;
            gridCells[i, j] = this;
            indexI = i;
            indexJ = j;
            nextPosition = new Vector2(i, j);
            tile.indexI = oldI;
            tile.indexJ = oldJ;
        }

    }
    public void SetMovePosition(int i,int j)
    {
        moveTile = true;
        gridCells[indexI, indexJ] = null;
        gridCells[i, j] = this;
        indexI = i;
        indexJ = j;
        nextPosition = new Vector2(i, j); 
    }
    /// <summary>
    /// TileEvents
    /// </summary>
    public TileEvent TileClick = new TileEvent();
    public TileEvent TileDrag = new TileEvent();
    public TileEvent TileUpClick = new TileEvent();
    public TileEvent TileMouseOver = new TileEvent();
    private void OnMouseDrag()
    {
        TileDrag.Invoke(this);
        //Debug.Log("OnMouseDragTile: " + indexI + " " + indexJ);
    }
    
    private void OnMouseUp()
    {
        TileUpClick.Invoke(this);
        //Debug.Log("OnMouseUpTile: " + indexI + " " + indexJ);

    }
    private void OnMouseDown()
    {
        TileClick.Invoke(this);
        //Debug.Log("OnMouseDownTile: " + indexI + " " + indexJ);
    }
    private void OnMouseOver()
    {
        TileMouseOver.Invoke(this);
        //Debug.Log("OnMouseOverTile: " + indexI + " " + indexJ);
    }

}
